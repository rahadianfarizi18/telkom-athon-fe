const buah = ['apel','mangga','jeruk','alpukat','melon','buah']
const sayur = ['kangkung','wortel','kol','brokolo','sawi','sayur']
const softdrink = ['coca-cola','coca-cola','pepsi','big cola','sprite','fanta']

let item = prompt('Masukkan makanan/minuman favorit anda:')

if(buah.includes(item)) item='buah'
if(sayur.includes(item)) item='sayur'
if(softdrink.includes(item)) item='softdrink'

console.log(item);

switch (item.toLowerCase()) {
    default:
        alert('Saya belum tahu makanan/minuman apa itu.')
        break;
    case 'sayur':
    case 'buah':
    case 'susu':
    case 'nasi':
    case 'telur':
    case 'air putih':
        alert('Sehat banget nih anaknya.')
        break;
    case 'softdrink':
    case 'burger':
    case 'pizza':
        alert('Kurang-kurangin ya.. gak sehat.')
}