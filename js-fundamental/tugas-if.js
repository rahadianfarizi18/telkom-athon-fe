// Psuedocode:
// var a

// input(a)

// while (a not number) do
//     input(a)
//     if (lebih dari 5 kali salah) then
//         output('yang bener ah')

// if (a = 0) then
//     output('Angka 0')
// else if (a habis dibagi 2) then
//     output('Angka genap')
// else
//     output('Angka ganjil')

function input_angka (){

    let a;
    a = Number(prompt("Masukkan sebuah angka: "));
    let counter = 0;
    while(isNaN(a)) {
        counter++;
        if (counter>5) alert('Yang bener ah.');
        a = prompt("Harus angka!\nMasukkan sebuah angka:");
        if (a === null) return;
    }
    if (a === null) return;

    if (a == 0) {
        alert('Angka nol');
    }
    else if (a%2 === 0) {
        alert('Angka genap');
    } else {
        alert('Angka ganjil');
    }

}

input_angka();
